import cv2, sys
import argparse, math
from six import text_type


parser = argparse.ArgumentParser(
 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--image', type=text_type, default = u'./lena.png',
   help='it represent the height of the kernal')
parser.add_argument('--height', type=text_type, default = u'8',
   help='it represent the height of the kernal')
parser.add_argument('--width', type=text_type, default = u'8',
   help='it represent the width of the kernal')
args = parser.parse_args()


def unpackPicture(img):
    row, col = 0, 0
    for line in img:
        for num in line:
            r[row][col] = num[0]
            g[row][col] = num[1]
            b[row][col] = num[2]
            col += 1
        row+=1
        col = 0
    pkg.append(r)
    pkg.append(g)
    pkg.append(b)
    return pkg


def BTC(pkg, imgHeight, imgWidth):
    kernalHeight = int(args.height)
    kernalWidth = int(args.width)
    print("h:{} w:{}".format(kernalHeight, kernalWidth))
    for channel in pkg:
        for y in range(0, imgHeight-kernalHeight, kernalHeight):
            for x in range(0, imgWidth-kernalWidth, kernalWidth):
                # print("y:{} x:{}".format(y, x))
                # calculate the value within the kernal matrix
                q = 0
                sumval = 0
                sumsquare = 0
                for ky in range(kernalHeight):
                    for kx in range(kernalWidth):
                        # print("ky:{} kx:{}".format(ky, kx))
                        sumval = sumval + channel[y+ky][x+kx]
                        sumsquare = sumsquare + channel[y+ky][x+kx]**2
                step1 = sumval/(kernalHeight * kernalWidth)
                step2 = sumsquare/(kernalHeight * kernalWidth)
                variance = math.sqrt(step2 - step1**2)
                # calculate the q(value that bigger than mean)
                for ky in range(kernalHeight):
                    for kx in range(kernalWidth):
                        q = q+1 if channel[y+ky][x+kx] >= step1 else q
                if q == 0 or q == kernalHeight * kernalWidth:
                    a = step1
                    b = step1
                else:
                    a = step1 - variance * math.sqrt(q/(kernalHeight * kernalWidth - q))
                    b = step1 + variance * math.sqrt((kernalHeight * kernalWidth - q)/q)

                for ky in range(kernalHeight):
                    for kx in range(kernalWidth):
                        channel[y+ky][x+kx] = b if channel[y+ky][x+kx] > step1 else a
        returnPkg.append(channel)

    return returnPkg

def packPicture(pkg):
    for y in range(0, height):
        for x in range(0, width):
            img[y][x] = [pkg[0][y][x],pkg[1][y][x],pkg[2][y][x]]
    return img

if __name__ == "__main__":

    # read the picture and feature
    img = cv2.imread(args.image)

    # cv2.imshow('image', img)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()

    height, width, channels = img.shape
    print("img : height:{} width:{}".format(len(img),len(img[0])))

    r = [[0 for a in range(width)] for b in range(height)]
    g = [[0 for a in range(width)] for b in range(height)]
    b = [[0 for a in range(width)] for b in range(height)]
    print("r : height:{} width:{}".format(len(r),len(r[0])))

    pkg = []
    returnPkg = []

    pkg = unpackPicture(img)
    returnPkg = BTC(pkg, height, width)
    img = packPicture(returnPkg)

    cv2.imshow('image', img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    cv2.imwrite("./BTCPicture.png",img)
