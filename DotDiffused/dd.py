import cv2, sys, math

def unpackPicture(img):
	row, col = 0, 0
	for line in img:
		for num in line:
			r[row][col] = num[0]
			g[row][col] = num[1]
			b[row][col] = num[2]
			col += 1
		row+=1
		col = 0
	pkg.append(r)
	pkg.append(g)
	pkg.append(b)
	return pkg

def genClassMatrix():
	classMatrix = [[34, 48, 40, 32, 29, 15, 23, 31],
				   [42, 58, 56, 53, 21,  5,  7, 10],
				   [50, 62, 61, 45, 13,  1,  2, 18],
				   [38, 46, 54, 37, 25, 17,  9, 26],
				   [28, 14, 22, 30, 35, 49, 41, 33],
				   [20,  4,  6, 11, 43, 59, 57, 52],
				   [12,  0,  3, 19, 51, 63, 60, 44],
				   [24, 16,  8, 27, 39, 47, 55, 36]]
	return classMatrix

def genDifMatrix():
	sumVal = 0
	difMatrix = [[1, 2, 1],
				 [2, 0, 2],
				 [1, 2, 1]]
	for a in range(len(difMatrix)):
		for b in range(len(difMatrix[0])):
			sumVal = sumVal + difMatrix[a][b]
	return difMatrix, sumVal

def DotDithering(pkg, difMatrix, classMatrix, imgHeight, imgWidth, sumVal):
	difMatrixHeight = len(difMatrix)
	difMatrixWidth = len(difMatrix[0])
	classMatrixHeight = len(classMatrix)
	classMatrixWidth = len(classMatrix[0])
	classMatrixTotal = classMatrixHeight * classMatrixWidth
	# print("cmh:{} xmw:{} total:{}".format(classMatrixHeight,classMatrixWidth,classMatrixTotal))
	recentPlace = []

	for channel in pkg:
		e = [[0 for a in range(imgWidth)] for b in range(imgHeight)]
		c = [[0 for a in range(imgWidth)] for b in range(imgHeight)]
		_y= [[0 for a in range(imgWidth)] for b in range(imgHeight)]
		s = [[0 for a in range(imgWidth)] for b in range(imgHeight)]
		for y in range(0, imgHeight, 8):
			for x in range(0, imgWidth, 8):
				if ((y + 8) > imgHeight) | ((x + 8) > imgWidth):
					pass
				else:
					# Go through the classMatrix
					for order in range(classMatrixTotal): #64
						
						# find the correct priority in classMatrix to operate
						for h in range(classMatrixHeight): #8
							for w in range(classMatrixWidth): #8
								if classMatrix[h][w] == order:
									recentPlace = [h,w]

						# calculate the s Value 
						for h in range(difMatrixHeight): #3
							for w in range(difMatrixWidth): #3
								if ((recentPlace[0] + (h - 1)) < 0) | ((recentPlace[0] + (h - 1)) >= classMatrixHeight) | ((recentPlace[1] + (w - 1)) < 0) | ((recentPlace[1] + (w - 1)) >= classMatrixWidth):
									pass
								else:
									s[y+recentPlace[0]][x+recentPlace[1]] = s[y+recentPlace[0]][x+recentPlace[1]] + e[y+recentPlace[0] + h - 1][x+recentPlace[1] + w - 1] * difMatrix[h][w] / sumVal
										# print("s:[{}][{}] e:[{}][{}] difMatrix[{}][{}] sumVal:{}".format(recentPlace[0],recentPlace[1],recentPlace[0]+h-1,recentPlace[1] + w - 1,h,w,sumVal))
									
						
						# retrieve proper c
						c[y+recentPlace[0]][x+recentPlace[1]] = channel[y+recentPlace[0]][x+recentPlace[1]] + s[y+recentPlace[0]][x+recentPlace[1]]
						# print("c:[{}][{}] channel:[{}][{}] s[{}][{}]".format(recentPlace[0],recentPlace[1],y+recentPlace[0],x+recentPlace[1],recentPlace[0],recentPlace[1]))

						#retrieve the e
						e[y+recentPlace[0]][x+recentPlace[1]] = c[y+recentPlace[0]][x+recentPlace[1]] - _y[y+recentPlace[0]][x+recentPlace[1]]

						# retrieve proper _y
						_y[y+recentPlace[0]][x+recentPlace[1]] = 255 if c[y+recentPlace[0]][x+recentPlace[1]] > 128 else 0
					
		returnPkg.append(_y)
	return returnPkg

def packPicture(pkg):
	for y in range(0, height):
			for x in range(0, width):
				img[y][x] = [pkg[0][y][x],pkg[1][y][x],pkg[2][y][x]]
	return img


if __name__ == "__main__":
	if len(sys.argv) != 2:
		print("Usage {} <image file>.".format(sys.argv[0]))
		exit(-1)
	else:	
		# read the picture and feature 
		img = cv2.imread(sys.argv[1])
		height, width, channels = img.shape
		print("height:{} width:{} channels:{}".format(height, width, channels))
		# print("img : height:{} width:{}".format(len(img),len(img[0])))

		r = [[0 for a in range(width)] for b in range(height)] 
		g = [[0 for a in range(width)] for b in range(height)] 
		b = [[0 for a in range(width)] for b in range(height)]
		# print("r : height:{} width:{}".format(len(r),len(r[0])))

		pkg = [] 
		returnPkg = [] 

		pkg = unpackPicture(img)
		difMatrix, sumVal = genDifMatrix()
		classMatrix = genClassMatrix()
		returnPkg = DotDithering(pkg, difMatrix, classMatrix, height, width, sumVal)
		
		img = packPicture(returnPkg)

		cv2.imshow('image', img)
		cv2.waitKey(0)
		cv2.destroyAllWindows()

		cv2.imwrite("./DDPicture.png",img)




