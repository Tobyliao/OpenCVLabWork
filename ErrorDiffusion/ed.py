import cv2, sys

def unpackPicture(img):
	row, col = 0, 0
	for line in img:
		for num in line:
			r[row][col] = num[0]
			g[row][col] = num[1]
			b[row][col] = num[2]
			col += 1
		row+=1
		col = 0
	pkg.append(r)
	pkg.append(g)
	pkg.append(b)
	return pkg


def errorDiffusion(pkg):
	for channel in pkg:
		for y in range(0, height-1):
			for x in range(1, width - 1):
				oldpixel = channel[y][x]
				channel[y][x] = 255 if oldpixel > 127 else 0
				quantError = oldpixel - channel[y][x]
				# print("x:{}, y:{}".format(x,y))
				channel[y  ][x+1] = channel[y  ][x+1] + 7/16.0 * quantError
				channel[y+1][x+1] = channel[y+1][x+1] + 5/16.0 * quantError
				channel[y+1][x  ] = channel[y+1][x  ] + 3/16.0 * quantError
				channel[y+1][x-1] = channel[y+1][x-1] + 1/16.0 * quantError
		returnPkg.append(channel)
	return returnPkg

def packPicture(pkg):
	for y in range(0, height):
			for x in range(0, width):
				img[y][x] = [pkg[0][y][x],pkg[1][y][x],pkg[2][y][x]]
	return img

if __name__ == "__main__":
	if len(sys.argv) != 2:
		print("Usage {} <image file>".format(sys.argv[0]))
		exit(-1)
	else:	
		# read the picture and feature 
		img = cv2.imread(sys.argv[1])
		
		# cv2.imshow('image', img)
		# cv2.waitKey(0)
		# cv2.destroyAllWindows()
		
		height, width, channels = img.shape
		print("img : height:{} width:{}".format(len(img),len(img[0])))

		r = [[0 for a in range(width)] for b in range(height)] 
		g = [[0 for a in range(width)] for b in range(height)] 
		b = [[0 for a in range(width)] for b in range(height)]
		print("r : height:{} width:{}".format(len(r),len(r[0])))

		pkg = [] 
		returnPkg = [] 

		pkg = unpackPicture(img)
		returnPkg = errorDiffusion(pkg)
		img = packPicture(returnPkg)

		cv2.imshow('image', img)
		cv2.waitKey(0)
		cv2.destroyAllWindows()

		cv2.imwrite("./EDPicture.png",img)


