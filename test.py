import cv2 
import numpy as np 
import matplotlib.pyplot as plt

#             						0
img = cv2.imread('pycv.png', cv2.IMREAD_GRAYSCALE)

# # IMREAD_COLOR = 1
# # IMREAD_UMCHANGE = -1
# # Method i
# cv2.imshow('image', img)
# cv2.waitKey(0)
# cv2.destroyAllWindows()

plt.imshow(img,cmap='gray')
# plt.imshow(img,cmap='gray')
# plt.plot([160,275],[110,115], 'black', linewidth=10)
plt.title('None')
plt.show()

# Crop Video
# # get the width and height of the video 
# cap = cv2.VideoCapture(0)

# width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
# height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))

# fourCC = cv2.VideoWriter_fourcc(*'avc1')
# out = cv2.VideoWriter('output.avi', fourCC, 24.0, (width, height))

# while (cap.isOpened()):
# 	ret, frame = cap.read()
# 	if ret == True:
# 		frame = cv2.flip(frame, 1)
# 		# gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
# 		out.write(frame)
# 		cv2.imshow('frame', frame)
# 		# cv2.imshow('gray', gray)
# 		if cv2.waitKey(1) & 0xFF == ord('q'):
# 			break
# 	else:
# 		break


	
# cap.release()
# out.release()
# cv2.destroyAllWindows()
