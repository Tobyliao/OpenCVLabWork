import cv2, sys
import argparse, math
from six import text_type


parser = argparse.ArgumentParser(
 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--image', type=text_type, default = u'./lena.png',
   help='it represent the path of the picture')
parser.add_argument('--height', type=text_type, default = u'8',
   help='it represent the height of the kernal')
parser.add_argument('--width', type=text_type, default = u'8',
   help='it represent the width of the kernal')
args = parser.parse_args()


def unpackPicture(img):
    row, col = 0, 0
    for line in img:
        for num in line:
            r[row][col] = num[0]
            g[row][col] = num[1]
            b[row][col] = num[2]
            col += 1
        row+=1
        col = 0
    pkg.append(r)
    pkg.append(g)
    pkg.append(b)
    return pkg


def EDBTC(pkg, imgHeight, imgWidth):
    kernalHeight = int(args.height)
    kernalWidth = int(args.width)
    for channel in pkg:
        c = [[0 for a in range(kernalWidth)] for b in range(kernalHeight)]
        e = [[0 for a in range(kernalWidth)] for b in range(kernalHeight)]
        s = [[0 for a in range(kernalWidth)] for b in range(kernalHeight)]
        for y in range(0,imgHeight-kernalHeight,kernalHeight):
            for x in range(0,imgWidth-kernalWidth,kernalWidth):
                sumVal = 0
                a = 256
                b = 0
                for ky in range(kernalHeight):
                    for kx in range(kernalWidth):
                        sumVal = sumVal + channel[y+ky][x+kx]
                        b = channel[y+ky][x+kx] if channel[y+ky][x+kx] > b else b
                        a = channel[y+ky][x+kx] if channel[y+ky][x+kx] < a else a
                mean = sumVal/(kernalHeight*kernalWidth)
                # with the value a, b, mean
                for ky in range(kernalHeight):
                    for kx in range(kernalWidth):
                        print("ky:{} kx:{}".format(ky, kx))
                        if ky == 0 or ky == 7 or kx == 0 or kx == 7 : 
                            print('ln')
                            pass
                        else:
                            s[ky][kx] = (e[ky][kx+1] * 7 + e[ky+1][kx+1] * 5 + e[ky+1][kx] * 3 + e[ky+1][kx-1]) / 16

                        c[ky][kx] = channel[y + ky][x + kx] + s[ky][kx]
                        channel[y + ky][x + kx] = b if c[ky][kx] >= mean else a
                        e[ky][kx] = c[ky][kx] - channel[y + ky][x + kx]

        returnPkg.append(channel)
    return returnPkg

def packPicture(pkg):
    for y in range(0, height):
        for x in range(0, width):
            img[y][x] = [pkg[0][y][x], pkg[1][y][x], pkg[2][y][x]]
    return img

if __name__ == "__main__":

    # read the picture and feature
    img = cv2.imread(args.image)

    # cv2.imshow('image', img)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()

    height, width, channels = img.shape
    print("img : height:{} width:{}".format(len(img),len(img[0])))

    r = [[0 for a in range(width)] for b in range(height)]
    g = [[0 for a in range(width)] for b in range(height)]
    b = [[0 for a in range(width)] for b in range(height)]
    print("r : height:{} width:{}".format(len(r),len(r[0])))

    pkg = []
    returnPkg = []

    pkg = unpackPicture(img)
    returnPkg = EDBTC(pkg, height, width)
    img = packPicture(returnPkg)

    cv2.imshow('image', img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

    cv2.imwrite("./EDBTCPicture.png",img)
