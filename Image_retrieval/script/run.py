import cv2, sys

def unpackPicture(img):
	row, col = 0, 0
	for line in img:
		for num in line:
			r[row][col] = num[0]
			g[row][col] = num[1]
			b[row][col] = num[2]
			col += 1
		row+=1
		col = 0
	pkg.append(r)
	pkg.append(g)
	pkg.append(b)
	return pkg

def genKernal(size):
	# Bayer-5 Kernal
	BayerKernal = [[0.513, 0.272, 0.724, 0.483, 0.543, 0.302, 0.694, 0.453],
				   [0.151, 0.755, 0.091, 0.966, 0.181, 0.758, 0.121, 0.936],
				   [0.634, 0.392, 0.574, 0.332, 0.664, 0.423, 0.604, 0.362],
				   [0.060, 0.875, 0.211, 0.815, 0.030, 0.906, 0.241, 0.845],
				   [0.543, 0.302, 0.694, 0.453, 0.513, 0.272, 0.724, 0.483],
				   [0.181, 0.758, 0.121, 0.936, 0.151, 0.755, 0.091, 0.966],
				   [0.664, 0.423, 0.604, 0.362, 0.634, 0.392, 0.574, 0.332],
				   [0.030, 0.906, 0.241, 0.845, 0.060, 0.875, 0.211, 0.815]]
	returnKernal = []
	KernalRow = []

	for h in range(0,int(size)):
		for w in range(0,int(size)):
			KernalRow.append(BayerKernal[h][w])
		returnKernal.append(KernalRow)
		KernalRow = []
	return returnKernal

def orderDithering(pkg, kernal, size):
	for channel in pkg:
		for y in range(0, height-1):
			for x in range(1, width - 1):
				m = y % int(size)
				n = x % int(size)
				channel[y][x] = 255 if channel[y][x] > kernal[m][n] * 255 else 0
		returnPkg.append(channel)
	return returnPkg

def packPicture(pkg):
	for y in range(0, height):
			for x in range(0, width):
				img[y][x] = [pkg[0][y][x],pkg[1][y][x],pkg[2][y][x]]
	return img


if __name__ == "__main__":
	if len(sys.argv) != 3:
		print("Usage {} <image file> <kernal size>.".format(sys.argv[0]))
		exit(-1)
	else:	
		# read the picture and feature 
		img = cv2.imread(sys.argv[1])
		kernalSize = sys.argv[2]
		
		# cv2.imshow('image', img)
		# cv2.waitKey(0)
		# cv2.destroyAllWindows()
		
		
		height, width, channels = img.shape
		# print("img : height:{} width:{}".format(len(img),len(img[0])))

		r = [[0 for a in range(width)] for b in range(height)] 
		g = [[0 for a in range(width)] for b in range(height)] 
		b = [[0 for a in range(width)] for b in range(height)]
		# print("r : height:{} width:{}".format(len(r),len(r[0])))

		pkg = [] 
		returnPkg = [] 

		pkg = unpackPicture(img)
		kernal = genKernal(kernalSize)
		returnPkg = orderDithering(pkg, kernal, kernalSize)
		img = packPicture(returnPkg)

		cv2.imshow('image', img)
		cv2.waitKey(0)
		cv2.destroyAllWindows()

		cv2.imwrite("./ODPicture.png",img)






